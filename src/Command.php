<?php

namespace Drupal\visithor;

use Drupal\visithor\UrlGenerator;

use Symfony\Component\Console\Output\ConsoleOutputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use Visithor\Command\GoCommand;
use Visithor\Executor\Executor;
use Visithor\Renderer\RendererFactory;

/**
 * Class Command
 */
class Command extends GoCommand {

  /**
   * @var UrlGenerator
   *
   * URL instances generator
   */
  protected $urlGenerator;

  /**
   * @var RendererFactory
   *
   * Renderer factory
   */
  protected $rendererFactory;

  /**
   * @var Executor
   *
   * Visithor Executor
   */
  protected $executor;

  /**
   * @var ConsoleOutputInterface
   *
   * Output
   */
  protected $output;

  /**
   * Construct
   *
   * @param UrlGenerator            $urlGenerator     Url generator
   * @param RendererFactory         $rendererFactory  Render factory
   * @param Executor                $executor         Executor
   * @param ConsoleOutputInterface  $output           Output
   */
  public function __construct(
    UrlGenerator $urlGenerator,
    RendererFactory $rendererFactory,
    Executor $executor,
    ConsoleOutputInterface $output
  ) {
    parent::__construct();
    $this->urlGenerator = $urlGenerator;
    $this->rendererFactory = $rendererFactory;
    $this->executor = $executor;
    $this->output = $output;
  }

  /**
   * Executes all business logic inside this command
   *
   * This method returns 0 if all executions passed. 1 otherwise.
   *
   * @param OutputInterface $output Output
   * @param array           $config Config
   * @param string          $format Format
   *
   * @return integer Execution return
   */
  public function go(array $config, $format) {
    $renderer = $this
      ->rendererFactory
      ->create($format);
    $this
      ->executor
      ->build();
    $urlChain = $this
      ->urlGenerator
      ->generate($config);
    $result = $this
      ->executor
      ->execute(
        $urlChain,
        $renderer,
        $this->output
      );
    $this
      ->executor
      ->destroy();
    return $result;
  }

}
