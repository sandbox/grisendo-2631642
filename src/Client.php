<?php

namespace Drupal\visithor;

use Drupal\Core\DrupalKernelInterface;

use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\HttpKernel\Client as CoreClient;

use Visithor\Client\Interfaces\ClientInterface;
use Visithor\Model\Url;

/**
 * Class Client
 */
class Client implements ClientInterface {

  /**
   * @var CoreClient
   *
   * Client
   */
  protected $client;

  /**
   * @var DrupalKernelInterface
   *
   * Kernel
   */
  protected $kernel;
  /**
   * @var SessionInterface
   *
   * Session
   */
  protected $session;

  /**
   * Construct
   *
   * @param SessionInterface $session
   * @param KernelInterface $kernel
   */
  public function __construct(
    SessionInterface $session,
    DrupalKernelInterface $kernel = null
  ) {
    $this->session = $session;
    $this->kernel = $kernel;
  }

  public function buildClient()
  {
    $this->kernel->boot();
    $this->client = $this
      ->kernel
      ->getContainer()
      ->get('visithor.core_client');
    $this->session->clear();
  }

  public function getResponseHTTPCode(Url $url) {
    try {
      $this->authenticate($url);
      $verb = $url->getOption('verb', 'GET');
      // Drupal kernel needs to be boot = false in order to make a new request.
      // Otherwise, it throws an exception:
      // 'Site path cannot be changed after calling boot()';
      $this->kernel->shutdown();
      $this
        ->client
        ->request($verb, $url->getPath());
      $result = $this
        ->client
        ->getResponse()
        ->getStatusCode();
    } catch (AccessDeniedHttpException $e) {
      $result = $e->getStatusCode();
    } catch (\Exception $e) {
      $result = 'ERR';
    }
    $this->expireAuthentication($url);
    return $result;
  }

  protected function authenticate(Url $url)
  {
    if (!$url->getOption('role')) {
      return $this;
    }
    /*$session = $this->session;
    $firewall = $url->getOption('firewall');
    $role = $url->getOption('role');
    $user = $this
      ->environmentBuilder
      ->getAuthenticationUser($url->getOption('role'));
    $token = new UsernamePasswordToken($user, null, $firewall, [$role]);
    $session->set('_security_' . $firewall, serialize($token));
    $session->save();
    $cookie = new Cookie(
      $session->getName(),
      $session->getId()
    );
    $this
      ->client
      ->getCookieJar()
      ->set($cookie);*/
    return $this;
  }

  protected function expireAuthentication(Url $url)
  {
    /*$session = $this->session;
    $session->remove('_security_' . $url->getOption('firewall'));
    $session->save();
    $this
      ->client
      ->getCookieJar()
      ->expire($session->getName());*/
    return $this;
  }

  public function destroyClient()
  {
    /*$this
      ->environmentBuilder
      ->tearDown($this->kernel);*/
  }

}
