<?php

use Symfony\Component\Console\Output\ConsoleOutput;

use Visithor\Reader\YamlConfigurationReader;
use Visithor\Visithor;

/**
 * Implements hook_drush_command().
 */
function visithor_drush_command() {
  $items = array();
  $items['visithor-go'] = array(
    'description' => 'Visit all defined urls.',
    'aliases' => array('vg'),
    'callback' => 'visithor_go_command'
  );
  return $items;
}

/**
 * Callback for the drush visithor-go command
 */
function visithor_go_command() {
  // TODO: Comma separated modules as an argument --modules
  // TODO: RENDERER_TYPE as an argument 'pretty'/'dots' --format
  $executor = \Drupal::service('visithor.commands');
  $reader = new YamlConfigurationReader();
  $config = $reader->readByFilename(__DIR__, 'visithor.yml');
  $executor->go($config, Visithor::RENDERER_TYPE_PRETTY);
}
